/* -*- mode: js2; -*- */
'use strict';
// --------------------------------------------- //
// ------- Random Game Kristofer Brink --------- //
// -------- Created by Nikhil Suresh ----------- //
// -------- Three.JS is by Mr. doob  ----------- //
// --------------------------------------------- //


// ------------------------------------- //
// ------- GLOBAL VARIABLES ------------ //
// ------------------------------------- //

/* http://stackoverflow.com/a/10590011/429091 */
require.config({
    catchError: true
    ,enforceDefine: true
    ,waitSeconds: 0
    ,xhtml: true
});

define('game', [
    'oo'
    ,'keyboard'
    ,'scene'
], function (
    oo
    ,Key
    ,sceneLoader
) {
// scene object variables
var renderer, scene, camera;
var playerBall;
var cameraPos = new THREE.Vector3(0,0,0);
var chancePerFrame = 10;
var colladaLoader = new THREE.ColladaLoader();
var body;

var extendTimer = 0;
var score = 0;
var scoreElem;
var unitLength = 20;
var tableLengthUnits = 30
var tableSide = unitLength * tableLengthUnits;

jQuery(document).ready(function () {
    scoreElem = jQuery('#score');
    body = jQuery('body');
    setup();
});


var myLoadImage, myLoadTexture;
(function () {
    var imageLoader = new THREE.ImageLoader();
    myLoadImage = function () {
        arguments[0] = arguments[0];
        return imageLoader.load.apply(imageLoader, arguments);
    };
    
    var textureLoader = new THREE.TextureLoader();
    textureLoader.crossOrigin = 'anonymous';
    myLoadTexture = function () {
        arguments[0] = arguments[0];
        return textureLoader.load.apply(textureLoader, arguments);
    };
})();

    


function setup()
{
    scoreElem.text('0');
    // Set up physics
	Physijs.scripts.worker = 'Scripts/physijs_worker.js';
    Physijs.scripts.ammo = 'ammo.js';

    // set up all the 3D objects in the scene	
	createScene();
	
   // and let's get cracking!
   draw();
}

function setSceneBasics(){
    // set the scene size
	var WIDTH = window.innerWidth;
	var HEIGHT = window.innerHeight;

	var c = document.getElementById("gameCanvas");

	// create a WebGL renderer, camera
	// and a scene
	renderer = new THREE.WebGLRenderer({antialias: true, logarithmicDepthBuffer: true});
    renderer.shadowMap.enabled = true;
    
    // Create camera
	camera =
	  new THREE.PerspectiveCamera(
		63, // View angle 
		WIDTH / HEIGHT, // Aspect
		0.1, // Near
		3000000); // Far
    // rotate to face towards the opponent
    
    


    
    scene = new Physijs.Scene();
    var winResize   = new THREEx.WindowResize(renderer, camera);

   

    // add the camera to the scene
	scene.add(camera);
	
	// set a default position for the camera
	// not doing this somehow messes up shadow rendering well this doesn't do anything says Kristofer Brink
    //camera.position.z = 200;
	
	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	c.appendChild(renderer.domElement);
}

function setGravity() {
    scene.setGravity(new THREE.Vector3( 0, -200, 0 ));
}

function createPlayer(){
    var playerBallMaterial = Physijs.createMaterial(
        new THREE.MeshLambertMaterial({ color: '#abcdef', transparent: true, opacity: .9 }),
        0, // no friction
        .0 // low restitution
    );


    playerBall = new Physijs.BoxMesh(
       new THREE.SphereGeometry(
          6, 
          100,
          100), 
        playerBallMaterial,
        1); // Gravity

    playerBall.receiveShadow = true;
    playerBall.castShadow = true;
    playerBall.position.set(0,1600,500);

    // // add the sphere to the scene
    scene.add(playerBall);
    // Slow down the ball
    playerBall.setDamping( .99, 0);

    playerBall.setAngularFactor(new THREE.Vector3(2,2,2));

    playerBall.rotation.set(new THREE.Vector3(0, 25, 90));

    var redMaterial = new THREE.MeshLambertMaterial({color: 0xff0000});
    var greenMaterial = new THREE.MeshLambertMaterial({color: 0x44ff44});
    playerBall.addEventListener('collision', function (other) {
        var otherUserData = other.userData || {};
        if (otherUserData.badguy) {
            other.material = redMaterial;
            //deltaScore(-1000);
        }
        if (otherUserData.goodguy) {
            other.material = greenMaterial;
            deltaScore(9000);
        }
    });
}

var light;
function createScene()
{

    setSceneBasics();
    
    // The island
    sceneLoader.loadScene('models/dried_eternal_springs.json').then(function (level) {
        console.log('got level');
        level.rotation.x = Math.PI / -2;
        level.children.forEach(function (child) {console.log('level contains ' + child.name);});
        scene.add(level);
    });
    
    createPlayer();
    
    setGravity();


    // MAGIC SHADOW CREATOR DELUXE EDITION with Lights PackTM DLC
    renderer.shadowMap.enabled = true;
    //renderer.shadowMapType = THREE.PCFSoftShadowMap; // snooth shadows

    
    
    scene.addEventListener('update', animate);
    createPointLight();
}


function createPointLight(){
    jQuery.map([
        {yOffset: function() { return playerBall.position.y + 100; }, xOffset: function(){ return playerBall.position.x - 100;}, zOffset: function(){return playerBall.position.z + 10;}
         , color: 0xffffff, distance: 2000, decay: .1, intensity: 2},
        
  ], function (lightInfo) {
        var light = new THREE.PointLight(
            lightInfo.color, 
            lightInfo.intensity || 1, // Intensity
            lightInfo.distance, // Distance
            lightInfo.decay); // decay
            light.castShadow = true;
            light.shadowCameraNear = 1;
            light.shadowCameraFar = tableSide;
            light.shadowDarkness = 0.5;
            light.shadowMapWidth = 2048;
            light.shadowMapHeight = 2048;
        
      var setLightPosition = function () {
          light.position.set(lightInfo.xOffset(), 
                             lightInfo.yOffset(), 
                             lightInfo.zOffset());
      };
      setLightPosition();
      scene.addEventListener('update', setLightPosition);
      scene.add(light);
  });
}



function draw()
{
	// Physijs simulate
    if(!Key.isDown(Key.P)) {
	   scene.simulate(); // run physics
    }
	// draw THREE.JS scene

	renderer.render(scene, camera);
    
    // loop draw function call
	requestAnimationFrame(draw);
}



// Handles player's movement
function animate()
{
    camera.position.set(playerBall.position.x, playerBall.position.y + 150, playerBall.position.z - 10);
    camera.lookAt(new THREE.Vector3(playerBall.position.x, playerBall.position.y, playerBall.position.z));
    // Keep player ball in the same place
    // --------------------- P  layer movement ------
    var impulse = new THREE.Vector3(0, 0, 0);
	if (Key.isDown(Key.D) || Key.isDown(Key.arrowRight)) {
		impulse.set(1, 0, 0);
    }
	if (Key.isDown(Key.A) || Key.isDown(Key.arrowLeft)) {
		impulse.set(-1, 0, 0);
	}
    if (Key.isDown(Key.W) || Key.isDown(Key.arrowRight)) {
		impulse.set(0, 0, -1);
    }
	if (Key.isDown(Key.S) || Key.isDown(Key.arrowLeft)) {
		impulse.set(0, 0, 1);
	}
    
    
    playerBall.applyCentralImpulse(impulse.multiplyScalar(4));
}

    return {};
});
