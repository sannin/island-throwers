/* -*- mode: js2; -*- */
'use strict';

define('heightfield', [
], function (
) {
    var fromGeometry;

    fromGeometry = function (geometry, material) {
        /*
         * There are just a bunch of vertices. We want to organize them
         * into a sort of grid and then import that grid into a PlaneMesh
         * so that physijs can do Heightfield magics. Just go through all
         * the vertices and store them into x offsets.
         */
        var rows = {};
        jQuery.map(geometry.vertices, function (vertex) {
            var row = rows[vertex.x];
            if (!row)
                rows[vertex.x] = row = [];
            row.push(vertex);
        });
        /* Validate that all rows have the same number of columns. */
        var columnCount;
        jQuery.map(rows, function (row) {
            if (!columnCount)
                columnCount = row.length;
            if (!columnCount)
                throw new Error('The row does not have any columns in it.');
            if (row.length != columnCount)
                throw new Error('Found row (' + row[0].x + ') with ' + row.length + ' columns even though we were expecting ' + columnCount + ' columns.');
        });
        rows = _.sortBy(rows, function (o) {
            return o[0].x;
        });
        /* <<length - 1>> because 1 line segment has two points -> one square has 4 points but 2 points along each segment. */
        var planeGeometry = new THREE.PlaneGeometry(
            rows.slice(-1)[0][0].x - rows[0][0].x,
            _.maxBy(rows[0], 'z').z - _.minBy(rows[0], 'z').z,
            rows.length - 1,
            rows[0].length - 1);
        var vertexXIndices = _(planeGeometry.vertices).map('x').sortBy().sortedUniq().invert().value();
        var vertexYIndices = _(planeGeometry.vertices).map('y').sortBy().sortedUniq().invert().value();
        var done = 0;
        _.forEach(planeGeometry.vertices, function (vertex) {
            var xi = vertexXIndices[vertex.x];
            var yi = vertexYIndices[vertex.y];
            if (xi === undefined || yi === undefined)
                throw new Error('Unable to find indexes for (' + vertex.x + ',' + vertex.y + ')');
            var row = rows[xi];
            if (row === undefined)
                throw new Error('row[' + xi + '] undefined!');
            var value = row[yi];
            if (value === undefined)
                throw new Error('rows[' + xi + '][' + yi + '] is undefined :-/. Bother!');
            vertex.z = value.y;
            if (done++ < 8)
                console.log(value.y);
        });
        planeGeometry.computeFaceNormals();
        planeGeometry.computeVertexNormals();

        var o = new Physijs.HeightfieldMesh(
            planeGeometry,
            material || Physijs.createMaterial(
                new THREE.MeshLambertMaterial({color: '#adf432'})),
            /* mass */ 0,
            rows.length - 1,
            rows[0].length - 1);
        o.position.set(
            _(planeGeometry.vertices).map('x').mean(),
            _(planeGeometry.vertices).map('y').mean(),
            0);
        console.log(o.position);

        return o;
    };

    /* Export things */
    return {        
        /**
         * \brief
         *   Given a geometry with vertices perfectly aligned in a grid with varying y values,
         *   generate and appropriately rotate and position a Physijs Heightfield.
         *
         * The provided geometry will only have its vertices inspected.
         * The x and y values of the vertices must be aligned on a grid.
         * Each x value must be associated with a constant number of vertices
         * and likewise for y. For this to work with Physijs, the number of
         * unique values for x must match the number of unique y values
         * (i.e., the heightfield must be square).
         *
         * The resultant heightfield will be positioned to match
         * the geometry. You must take care when repositioning the
         * Object3D to make repositioning relative to the initial
         * values.
         *
         * \returns
         *   A Physijs Heightfield.
         */
        fromGeometry: fromGeometry
    };
});
