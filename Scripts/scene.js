/* -*- mode: js2; -*- */
'use strict';

define('scene', [
    'heightfield'
    ,'oo'
], function (
    heightfield
    ,oo
) {
    var MeshTypeInfo;
    var TypeInfo;
    var objectLoader;
    var loadObject;
    var toPhysijsObject;
    var typeInfos;

    loadObject = function (path) {
        objectLoader = objectLoader || new THREE.ObjectLoader();
        
        return new Promise(function (success, reject) {
            try
            {
                objectLoader.load(path, success);
            } 
            catch (ex)
            {
                reject(ex);
            }
        });
    };

    TypeInfo = function (test, build) {
        this.test = test;
        this.build = build;
    };

    MeshTypeInfo = oo.buildClass(TypeInfo, function (superPrototype, superConstructor) {
        return {
            constructor: function (regexp, type) {
                if (!regexp)
                    throw new Error('regexp is required');
                if (!type)
                    throw new Error('type is required');
                superConstructor.call(
                    this,
                    function (obj) {
                        /* 
                         * Use the name/label set in Blender to infer
                         * the Physijs thing to use.
                         */
                        return regexp.test(obj.name);
                    },
                    function (obj) {
                        var o = new type(obj.geometry, obj.material);
                        o.name = obj.name;
                        console.log('wrapped ' + obj.name);
                        return o;
                    });
            }
        };
    });

    typeInfos = [
        /*
         * Treat the Scene itself or general anonymous grouping object
         * as grouping objects.
         */
        new TypeInfo(function (obj) {
            return !obj.name;
        }, function () {
            return new THREE.Object3D();
        })
        ,new MeshTypeInfo(/Cube/, Physijs.BoxMesh)
        ,new MeshTypeInfo(/Sphere/, Physijs.SphereMesh)
        ,new MeshTypeInfo(/Cylinder/, Physijs.CylinderMesh)
        ,new MeshTypeInfo(/Cone/, Physijs.ConeMesh)
        ,new MeshTypeInfo(/Capsule/, Physijs.CapsuleMesh)
        ,new TypeInfo(function (obj) {
            return /Grid/.test(obj.name);
        }, function (obj) {
            var o = heightfield.fromGeometry(obj.geometry/*, obj.material*/);
            o.name = obj.name;
            return o;
        })
        /*
         * Ignore object marked Ignore.
         */
        ,new TypeInfo(function (obj) {
            return /Ignore/.test(obj.name);
        }, function () {})
    ];

    toPhysijsObject = function (obj) {
        var o;
        var typeInfo;
        console.log('processing ' + obj.name);

        typeInfo = _.find(typeInfos, function (typeInfo) {
            return typeInfo.test(obj);
        });
        if (!typeInfo)
            throw new Error('Object in imported scene with name ' + obj.name + ' does not match any known pattern.');
        o = typeInfo.build(obj);
        if (!o)
            return o;
        _.forEach(obj.children, function (child) {
            var childO = toPhysijsObject(child);
            if (childO)
                o.add(childO);
        });
        return o;
    };

    return {
        loadScene: function (path) {
            return loadObject(path).then(function (obj, materials) {
                return toPhysijsObject(obj, materials);
            });
        }
    };
});
