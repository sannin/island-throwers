/* -*- mode: js2; -*- */
'use strict';

define('oo', {
    buildClass: function (
        superClass,
        build) {
        var superPrototype = (superClass || {}).prototype || null;
        var ingredients = build(superPrototype, superClass);
        if (!ingredients.constructor)
            throw new Error('You must return an object with a constructor property.');
        ingredients.constructor.prototype = Object.create(superPrototype);
        jQuery.extend(ingredients.constructor.prototype, ingredients.members);
        return ingredients.constructor;
    }
});

requirejs([
    'oo'
], function (
    oo
) {
    var Super = oo.buildClass(
        undefined,
        function (superPrototype) {
           return {
               constructor: function () {},
               members: {
                   squawk: function () {
                       /* Do something specific to the superclass */
                   }
               }
           };
        });
    var Sub = oo.buildClass(
        Super,
        function (superPrototype, superConstructor) {
            return {
                constructor: function () {
                    /* Call super constructor */
                    superConstructor();
                },
                members: {
                    squawk: function () {
                        /* Do something specific to the subclass */
                        
                        /* Call the base implementation if it makes sense to do so */
                        superPrototype.squawk.call(this);
                    }
                }
            };
        });
    var x = new Super();
    x.squawk();
    x = new Sub();
    x.squawk();
});
