/* -*- mode: js2; -*- */
'use strict';

define('keyboard', function () {
    var Key = {
      _pressed: {},

      isDown: function(keyCode) {
        return !!this._pressed[keyCode];
      },

      press: function(which) {
        this._pressed[which] = true;
      },

      release: function(which) {
        this._pressed[which] = undefined;
      }
    };

    (function () {
        var i, c;
        var keys = 'WASD ZXCVFIJKLP';
        for (var i in keys)
        {
            c = keys[i];
            Key[c] = c.charCodeAt(0);
        }
        /* Arrow keys: http://stackoverflow.com/a/6227035 */
        Key.arrowLeft = 37;
        Key.arrowUp = 38;
        Key.arrowRight = 39;
        Key.arrowDown = 40;
    })();

    jQuery(document).on(
        'keydown',
        function (e) {
           Key.press(e.which);
        });
    jQuery(document).on(
        'keyup',
        function (e) {
            Key.release(e.which);
        });
    return Key;
});
